import 'package:flutter/material.dart';
import 'screen/Login/Login_Screen.dart';
import 'screen/Login/HomeScreen.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(primaryColor: Colors.purple),
    debugShowCheckedModeBanner: false,
    home: HomeScreen(),
  ));
}
