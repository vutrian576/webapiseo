import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:myapp/screen/Login/Login_Screen.dart';
import 'KeywordScreen.dart';
import 'dart:html' as html;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class Categories {
  final String CriterionID;
  final String Category;

  Categories({
    required this.CriterionID,
    required this.Category,
  });

  factory Categories.fromJson(json) {
    return Categories(
        CriterionID: json['CriterionID'], Category: json['Category']);
  }
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController textSearchInput = new TextEditingController();
  final TextEditingController startPage = new TextEditingController();

  late Future<List<Categories>> futureAlbum;
  late int countKeyword = 0;
  late int countPage = 0;
  late int currentPage = 0;
  @override
  void initState() {
    super.initState();
    futureAlbum = oki("");
  }

  Widget nextTrang() {
    if (currentPage == 1 || currentPage == 0) {
      return Row(
        children: [
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Text("Trang hiện tại: ${currentPage}"),
          ),
          ElevatedButton(
            child: Text("Next"),
            onPressed: () => {
              setState(() {
                currentPage = currentPage + 1;
                startPage.text = '$currentPage';
                futureAlbum = oki(textSearchInput.text);
              })
            },
          ),
        ],
      );
    } else if (currentPage > 1 && currentPage < countPage) {
      return Row(
        children: [
          ElevatedButton(
              child: Text("Preview"),
              onPressed: () => {
                    setState(() {
                      currentPage = currentPage - 1;
                      startPage.text = '$currentPage';
                      futureAlbum = oki(textSearchInput.text);
                    })
                  }),
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Text("Trang hiện tại: ${currentPage}"),
          ),
          ElevatedButton(
            child: Text("Next"),
            onPressed: () => {
              setState(() {
                currentPage = currentPage + 1;
                startPage.text = '$currentPage';
                futureAlbum = oki(textSearchInput.text);
              })
            },
          ),
        ],
      );
    } else {
      return Row(
        children: [
          ElevatedButton(
              child: Text("Preview"),
              onPressed: () => {
                    setState(() {
                      currentPage = currentPage - 1;
                      startPage.text = '$currentPage';
                      futureAlbum = oki(textSearchInput.text);
                    })
                  }),
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Text("Trang hiện tại: ${currentPage}"),
          ),
        ],
      );
    }
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );
  Future<List<Categories>> oki(String stringSearch) async {
    Map<String, String> connection = {
      "PageStart": "1",
      "sizePage": "50",
      "nameCategories": stringSearch
    };
    if (startPage.text != "") {
      connection['PageStart'] = startPage.text;
    } else {
      connection['PageStart'] = "1";
    }
    var uri = "http://23.172.112.93:8010/private/categories";
    http.Response r = await http.post(Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'access_token': html.window.localStorage["csrf"] ?? ""
        },
        body: jsonEncode(connection));

    if (r.statusCode == 200) {
      setState(() {
        countKeyword = json.decode(utf8.decode(r.bodyBytes))["count"];
        countPage =
            (countKeyword / int.parse(connection['sizePage'] ?? "50")).ceil();
        currentPage = int.parse(connection['PageStart'] ?? "1");
      });
      return (json.decode(json.decode(r.body)["data"]) as List)
          .map((data) => Categories.fromJson(data))
          .toList();
    } else if (r.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginScreen()));
    } else {
      displayDialog(context, "NOT FOUND", "Không tìm thấy bản ghi phù hợp");
    }
    throw Exception('Failed to load album');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView(children: [
                SizedBox(height: 20),
                TextField(
                    controller: textSearchInput,
                    decoration:
                        InputDecoration(labelText: "Vui lòng nhập từ khóa")),
                SizedBox(height: 20),
                TextField(
                    controller: startPage,
                    decoration: InputDecoration(labelText: "Start Page")),
                SizedBox(height: 20),
                ElevatedButton(
                  child: Text("Tìm kiếm"),
                  onPressed: () => {
                    setState(() {
                      futureAlbum = oki(textSearchInput.text);
                    })
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () => {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  KeywordScreen(categoryID1: "all"),
                              settings: RouteSettings(
                                arguments: "all",
                              ),
                            ),
                          )
                        },
                    child: Text("Tìm kiếm trên tất cả các categories")),
                new Row(children: <Widget>[
                  nextTrang(),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số trang: ${countPage}"),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số truy vấn: ${countKeyword}"),
                  ),
                ]),
                Center(
                  child: FutureBuilder<List<Categories>>(
                      future: futureAlbum,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return buid_data_table(snapshot.data);
                        }
                        return CircularProgressIndicator();
                      }),
                ),
                new Row(children: <Widget>[
                  nextTrang(),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số trang: ${countPage}"),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số truy vấn: ${countKeyword}"),
                  ),
                ]),
              ]),
            )));
  }

  Widget buid_data_table(List<Categories>? ok1) {
    final columns = ["CriterionID", "Category"];
    return DataTable(columns: getColumn(columns), rows: getRow(ok1));
  }

  List<DataColumn> getColumn(List<String> columns) =>
      columns.map((String column) => DataColumn(label: Text(column))).toList();

  List<DataRow> getRow(List<Categories>? categories) =>
      categories?.map((Categories category) {
        final cells = [category.CriterionID, category.Category];
        return DataRow(
          cells: getCell(cells),
          onSelectChanged: (newValue) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    KeywordScreen(categoryID1: category.CriterionID),
                settings: RouteSettings(
                  arguments: category.CriterionID,
                ),
              ),
            );
          },
        );
      }).toList() ??
      [];

  List<DataCell> getCell(List<String> cells) =>
      cells.map((String cell) => DataCell(Text(cell))).toList();
}
