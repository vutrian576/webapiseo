import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:html' as html;
import 'package:myapp/screen/Login/Login_Screen.dart';

class KeywordScreen extends StatefulWidget {
  final String categoryID1;
  KeywordScreen({Key? key, required this.categoryID1}) : super(key: key);

  @override
  _KeywordScreen createState() => _KeywordScreen(categoryID: categoryID1);
}

class Keyword {
  final String keyword;
  final dynamic location;
  final dynamic search_volume;
  final dynamic cpc;
  final dynamic time_update;
  final dynamic daily_impressions_avg;
  final dynamic daily_impressions_min;
  final dynamic daily_impressions_max;
  final dynamic daily_clicks_avg;
  final dynamic daily_clicks_min;
  final dynamic daily_clicks_max;
  Keyword({
    required this.keyword,
    required this.location,
    required this.search_volume,
    required this.cpc,
    required this.time_update,
    required this.daily_impressions_avg,
    required this.daily_impressions_min,
    required this.daily_impressions_max,
    required this.daily_clicks_avg,
    required this.daily_clicks_min,
    required this.daily_clicks_max,
  });
  factory Keyword.fromJson(json) {
    return Keyword(
        keyword: json['keyword'],
        location: json['location'],
        search_volume: json['search_volume'],
        cpc: json['cpc'],
        time_update: json['time_update'],
        daily_impressions_avg: json['daily_impressions_avg'],
        daily_impressions_min: json['daily_impressions_min'],
        daily_impressions_max: json['daily_impressions_max'],
        daily_clicks_avg: json['daily_clicks_avg'],
        daily_clicks_min: json['daily_clicks_min'],
        daily_clicks_max: json['daily_clicks_max']);
  }
}

class _KeywordScreen extends State<KeywordScreen> {
  late String categoryID = "10003";

  _KeywordScreen({required this.categoryID});

  Map<String, String> connectionFields = {
    "Search volume": "keyword_info.search_volume",
    "cpc": "keyword_info.cpc",
    "Daily Impressions avg": "impressions_info.daily_impressions_avg",
    "Daily Impressions Min": "impressions_info.daily_impressions_min",
    "Daily Impressions Max": "impressions_info.daily_impressions_max",
    "Daily Clicks Avg": "impressions_info.daily_click_avg",
    "Daily Clicks Min": "impressions_info.daily_click_min",
    "Daily Clicks Max": "impressions_info.daily_click_max",
    "Tăng dần": "asc",
    "Giảm dần": "desc",
  };
  final TextEditingController textSearchInput = new TextEditingController();
  final TextEditingController startPage = new TextEditingController();
  final TextEditingController endPage = new TextEditingController();
  final TextEditingController sizePage = new TextEditingController();
  late int countKeyword = 0;
  late int countPage = 0;
  late int currentPage = 0;

  late String _selectedFeild1 = '';
  late String _selectedFeild2 = '';

  late String _typesort = '';
  final TextEditingController minRange = new TextEditingController();
  final TextEditingController maxRange = new TextEditingController();

  late Future<List<Keyword>> futureAlbum;
  @override
  void initState() {
    super.initState();
    futureAlbum = oki();
    print(categoryID);
  }

  Widget nextTrang() {
    if (currentPage == 1 || currentPage == 0) {
      return Row(
        children: [
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Text("Trang hiện tại: ${currentPage}"),
          ),
          ElevatedButton(
            child: Text("Next"),
            onPressed: () => {
              setState(() {
                currentPage = currentPage + 1;
                startPage.text = '$currentPage';
                futureAlbum = oki();
              })
            },
          ),
        ],
      );
    } else if (currentPage > 1 && currentPage < countPage) {
      return Row(
        children: [
          ElevatedButton(
              child: Text("Preview"),
              onPressed: () => {
                    setState(() {
                      currentPage = currentPage - 1;
                      startPage.text = '$currentPage';
                      futureAlbum = oki();
                    })
                  }),
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Text("Trang hiện tại: ${currentPage}"),
          ),
          ElevatedButton(
            child: Text("Next"),
            onPressed: () => {
              setState(() {
                currentPage = currentPage + 1;
                startPage.text = '$currentPage';
                futureAlbum = oki();
              })
            },
          ),
        ],
      );
    } else {
      return Row(
        children: [
          ElevatedButton(
              child: Text("Preview"),
              onPressed: () => {
                    setState(() {
                      currentPage = currentPage - 1;
                      startPage.text = '$currentPage';
                      futureAlbum = oki();
                    })
                  }),
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(3.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: Text("Trang hiện tại: ${currentPage}"),
          ),
        ],
      );
    }
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );
  Future<Null> ok2() async {
    Map<String, String> connection = {
      'PageStart': "1",
      "sizePage": "50",
      "CategoriesID": categoryID,
    };
    if (categoryID != "") {
      connection['categoryID'] = categoryID;
    }
    if (_selectedFeild1 != "" && _typesort != '') {
      connection['sortByKey'] = connectionFields[_selectedFeild1] ?? "";
      connection['sortType'] = connectionFields[_typesort] ?? "";
    }
    if (_selectedFeild2 != "" && (minRange.text != '' || maxRange.text != '')) {
      connection['RangeField'] = connectionFields[_selectedFeild2] ?? "";
      connection['RangeStart'] = minRange.text != "" ? minRange.text : "0";
      connection['RangeEnd'] =
          maxRange.text != "" ? maxRange.text : "100000000";
    }
    if (textSearchInput.text != "") {
      connection['TextInKW'] = textSearchInput.text;
    }
    if (startPage.text != "") {
      connection['PageStart'] = startPage.text;
    } else {
      connection['PageStart'] = "1";
    }
    if (endPage.text != "") {
      connection['PageEnd'] = endPage.text;
    } else {
      connection['PageEnd'] = connection['PageStart'] ?? "1";
    }
    if (sizePage.text != "") {
      connection['sizePage'] = sizePage.text;
    } else {
      connection['sizePage'] = "50";
    }
    var uri = "http://23.172.112.93:8010/private/getfilekeyword";
    http.Response r = await http.post(Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'access_token': html.window.localStorage["csrf"] ?? ""
        },
        body: jsonEncode(connection));

    if (r.statusCode == 200) {
      final blob = html.Blob([r.bodyBytes]);
      final url = html.Url.createObjectUrlFromBlob(blob);
      final anchor = html.document.createElement('a') as html.AnchorElement
        ..href = url
        ..style.display = 'none'
        ..download = "vutrian.zip";
      html.document.body!.children.add(anchor);

      anchor.click();

      html.document.body!.children.remove(anchor);
      html.Url.revokeObjectUrl(url);
    } else if (r.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginScreen()));
    } else if (r.statusCode == 408) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      displayDialog(
          context, "OVER SIZE", "Kích thước tải về lớn hơn 50000 bản ghi");
    } else {
      displayDialog(context, "NOT FOUND", "Không tìm thấy bản ghi phù hợp");
    }
  }

  Future<List<Keyword>> oki() async {
    Map<String, String> connection = {
      'PageStart': "1",
      "sizePage": "50",
      "CategoriesID": categoryID,
    };
    if (categoryID != "") {
      connection['categoryID'] = categoryID;
    }
    if (_selectedFeild1 != "" && _typesort != '') {
      connection['sortByKey'] = connectionFields[_selectedFeild1] ?? "";
      connection['sortType'] = connectionFields[_typesort] ?? "";
    }
    if (_selectedFeild2 != "" && (minRange.text != '' || maxRange.text != '')) {
      connection['RangeField'] = connectionFields[_selectedFeild2] ?? "";
      connection['RangeStart'] = minRange.text != "" ? minRange.text : "0";
      connection['RangeEnd'] = maxRange.text != "" ? maxRange.text : "10000000";
    }
    if (textSearchInput.text != "") {
      connection['TextInKW'] = textSearchInput.text;
    }
    if (startPage.text != "") {
      connection['PageStart'] = startPage.text;
    } else {
      connection['PageStart'] = "1";
    }
    if (endPage.text != "") {
      connection['PageEnd'] = endPage.text;
    } else {
      connection['PageEnd'] = connection['PageStart'] ?? "1";
    }
    if (sizePage.text != "") {
      connection['sizePage'] = sizePage.text;
    } else {
      connection['sizePage'] = "50";
    }
    var uri = "http://23.172.112.93:8010/private/keyword";
    http.Response r = await http.post(Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'access_token': html.window.localStorage["csrf"] ?? ""
        },
        body: jsonEncode(connection));

    if (r.statusCode == 200) {
      print(r.body);
      setState(() {
        countKeyword = json.decode(utf8.decode(r.bodyBytes))["count"];
        countPage =
            (countKeyword / int.parse(connection['sizePage'] ?? "50")).ceil();
        currentPage = int.parse(connection['PageStart'] ?? "1");
      });

      return (json.decode(json.decode(utf8.decode(r.bodyBytes))["data"])
              as List)
          .map((data) => Keyword.fromJson(data))
          .toList();
    } else if (r.statusCode == 401) {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginScreen()));
      throw Exception('Failed to load album');
    } else {
      displayDialog(context, "NOT FOUND", "Không tìm thấy bản ghi phù hợp");
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView(children: [
                SizedBox(height: 20),
                new Row(children: <Widget>[
                  SizedBox(width: 20),
                  Flexible(
                      child: TextField(
                          controller: textSearchInput,
                          decoration: InputDecoration(
                              labelText: "Vui lòng nhập từ khóa"))),
                  SizedBox(width: 20),
                  Flexible(
                      child: TextField(
                          controller: startPage,
                          decoration:
                              InputDecoration(labelText: "Start Page"))),
                  SizedBox(width: 20),
                  Flexible(
                      child: TextField(
                          controller: endPage,
                          decoration: InputDecoration(labelText: "End Page"))),
                  SizedBox(width: 20),
                  Flexible(
                      child: TextFormField(
                          controller: sizePage,
                          decoration: InputDecoration(labelText: "Page Size"))),
                ]),
                new Row(children: <Widget>[
                  SizedBox(width: 20),
                  Text("Sắp xếp:", style: TextStyle(fontSize: 20)),
                  SizedBox(width: 20),
                  Flexible(
                      child: new DropdownButton<String>(
                          hint: Text("Field"),
                          value: _selectedFeild1,
                          items: <String>[
                            "",
                            "Search volume",
                            "cpc",
                            "Daily Impressions avg",
                            "Daily Impressions Min",
                            "Daily Impressions Max",
                            "Daily Clicks Avg",
                            "Daily Clicks Min",
                            "Daily Clicks Max"
                          ].map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (val) async {
                            setState(() {
                              _selectedFeild1 = val ?? "";
                            });
                          })),
                  SizedBox(width: 20),
                  Flexible(
                      child: new DropdownButton<String>(
                          hint: Text("Type"),
                          value: _typesort,
                          items: <String>["", 'Tăng dần', 'Giảm dần']
                              .map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (val) async {
                            setState(() {
                              _typesort = val ?? "";
                            });
                          })),
                  SizedBox(width: 40),
                  Text("Range:", style: TextStyle(fontSize: 20)),
                  SizedBox(width: 20),
                  Flexible(
                      child: new DropdownButton<String>(
                          hint: Text("Field"),
                          value: _selectedFeild2,
                          items: <String>[
                            "",
                            "Search volume",
                            "cpc",
                            "Daily Impressions avg",
                            "Daily Impressions Min",
                            "Daily Impressions Max",
                            "Daily Clicks Avg",
                            "Daily Clicks Min",
                            "Daily Clicks Max"
                          ].map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (val) async {
                            setState(() {
                              _selectedFeild2 = val ?? "";
                            });
                          })),
                  SizedBox(width: 20),
                  Flexible(
                      child: TextFormField(
                          controller: minRange,
                          decoration: InputDecoration(labelText: "Min Value"))),
                  SizedBox(width: 20),
                  Flexible(
                      child: TextFormField(
                          controller: maxRange,
                          decoration: InputDecoration(labelText: "Max Value"))),
                  SizedBox(height: 40),
                  SizedBox(width: 20),
                  ElevatedButton(
                    child: Text("Tìm kiếm"),
                    onPressed: () => {
                      setState(() {
                        futureAlbum = oki();
                      })
                    },
                  ),
                  ElevatedButton(
                    child: Text("Tải xuống"),
                    onPressed: () => {ok2()},
                  ),
                ]),
                SizedBox(height: 20),
                new Row(children: <Widget>[
                  nextTrang(),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số trang: ${countPage}"),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số truy vấn: ${countKeyword}"),
                  ),
                ]),
                Center(
                    child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: FutureBuilder<List<Keyword>>(
                      future: futureAlbum,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return buid_data_table(snapshot.data);
                        }
                        return CircularProgressIndicator();
                      }),
                )),
                new Row(children: <Widget>[
                  nextTrang(),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số trang: ${countPage}"),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    padding: const EdgeInsets.all(3.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueAccent)),
                    child: Text("Tổng số truy vấn: ${countKeyword}"),
                  ),
                ]),
              ]),
            )));
  }

  Widget buid_data_table(List<Keyword>? ok1) {
    final columns = [
      "Keyword",
      "Location",
      "Search volume",
      "cpc",
      "Time Update",
      "Daily Impressions avg",
      "Daily Impressions Min",
      "Daily Impressions Max",
      "Daily Clicks Avg",
      "Daily Clicks Min",
      "Daily Clicks Max"
    ];
    return DataTable(
        showBottomBorder: true,
        headingRowColor:
            MaterialStateColor.resolveWith((states) => Color(0xFFFAA7B8)),
        columns: getColumn(columns),
        rows: getRow(ok1));
  }

  List<DataColumn> getColumn(List<String> columns) => columns
      .map((String column) => DataColumn(
            label: Text(
              column,
              maxLines: 3,
              softWrap: false,
              overflow: TextOverflow.fade,
            ),
          ))
      .toList();

  List<DataRow> getRow(List<Keyword>? categories) =>
      categories?.map((Keyword category) {
        final cells = [
          category.keyword,
          category.location,
          category.search_volume,
          category.cpc,
          category.time_update,
          category.daily_impressions_avg,
          category.daily_impressions_min,
          category.daily_impressions_max,
          category.daily_clicks_avg,
          category.daily_clicks_min,
          category.daily_clicks_max,
        ];
        return DataRow(
          cells: getCell(cells),
        );
      }).toList() ??
      [];

  List<DataCell> getCell(List<dynamic> cells) => cells
      .map((dynamic cell) => DataCell(
            Text(
              '${cell}',
              maxLines: 3,
              softWrap: false,
              overflow: TextOverflow.fade,
            ),
          ))
      .toList();
}
