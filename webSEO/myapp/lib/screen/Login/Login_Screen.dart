import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'HomeScreen.dart';
import 'dart:html' as html;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController usernameController = new TextEditingController();

  final TextEditingController passwordController = new TextEditingController();
  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );
  Future<Null> _login(String username, String password) async {
    Map<String, String> connection = {
      'grant_type': 'password',
      "username": username,
      "password": password,
    };
    var uri = "http://23.172.112.93:8010/public/token";
    http.Response r = await http.post(Uri.parse(uri),
        headers: {"content-type": "application/x-www-form-urlencoded"},
        body: connection);

    if (r.statusCode == 200) {
      final jwt = json.decode(r.body)["access_token"] ?? "";
      html.window.localStorage["csrf"] = jwt;

      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      displayDialog(context, "An Error Occurred",
          "No account was found matching that username and password");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomRight,
                colors: [Colors.red, Colors.pink])),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 80,
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "Chào mừng đến với AutoSEO",
              style: TextStyle(color: Colors.white, fontSize: 40),
            ),
            SizedBox(
              height: 30,
            ),
            Expanded(
                child: Container(
              constraints: BoxConstraints(
                  minWidth: 400, maxWidth: 480, maxHeight: 560, minHeight: 300),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 60),
                  Text(
                    "Đăng nhập",
                    style: TextStyle(color: Colors.black, fontSize: 30),
                  ),
                  SizedBox(height: 20),
                  Container(
                    width: 350,
                    child: TextField(
                      controller: usernameController,
                      decoration: InputDecoration(labelText: 'User Name'),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: 350,
                    child: TextField(
                      obscureText: true,
                      controller: passwordController,
                      decoration: InputDecoration(labelText: 'Password'),
                    ),
                  ),
                  SizedBox(height: 60),
                  GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      width: 250,
                      child: ElevatedButton(
// double.infinity is the width and 30 is the height
                        onPressed: () => {
                          _login(
                              usernameController.text, passwordController.text)
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(fontSize: 30),
                        ),
                        style: ElevatedButton.styleFrom(
                            primary: Colors.redAccent,
                            padding: EdgeInsets.symmetric(
                                horizontal: 50, vertical: 20),
                            textStyle: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                      ),
                    ),
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    ));
  }
}
