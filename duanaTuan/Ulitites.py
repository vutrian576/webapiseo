import pandas as pd
class Utilies:
    @staticmethod
    def ExportFile(jsondata,fileExportType):
        dataFrame = pd.DataFrame.from_dict(jsondata, orient="index")
        dataFrame.to_csv("dataqueryfile/data.{}".format(fileExportType))
        return "dataqueryfile/data.{}".format(fileExportType)