from typing import List, Optional
from settings import HOST
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from settings import *
import os, uuid
import pandas as pd
import csv
import time
from QueryStatement import QueryStatement
from QueryBase import *
from pandas.io.json import json_normalize
import sys
class ImportData:

    def __init__(self):
        self.host =HOST
        self.port = PORT
        if HASPASS:
            self.username = USER_NAME
            self.password = USER_PASS
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port,'timeout':6000}],http_auth=(self.username, self.password))
        else:
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port ,'timeout':6000}])

    def reset(self):
        if HASPASS:
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port,'timeout':6000}],http_auth=(self.username, self.password))
        else:
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port,'timeout':6000}])
        
    def insert_data_by_bulk(self,data,chunk_size=5000):
        res = helpers.bulk(self.elasticsearch, data,chunk_size=chunk_size,max_backoff=1000)
    def json_iterator(self,filename, _index, doc_type):
        i =0
        with open(filename,'r',encoding='utf-8') as f:
            for doc in f:
                i=i+1
                print(i)
                if '{"index"' not in doc:
                    yield {
                        "_index": _index,
                        "_type": doc_type,
                        "_id": uuid.uuid4(),
                        "_source": doc
                }
    def csv_iterator(self,filename,fieldsName:list, _index, doc_type):
        with open(filename,'r',encoding='utf-8') as f:
            i=0
            reader = csv.DictReader(f,fieldsName)
            for doc in reader:
                i=i+1
                print(i)
                if '{"index"' not in doc:
                    yield {
                        "_index": _index,
                        "_type": doc_type,
                        "_id": uuid.uuid4(),
                        "_source": doc
                }
if __name__ == "__main__":
    a = ImportData()
    a.elasticsearch.indices.put_settings(index="_all",body= {"max_result_window" : 25000000})
    filename  = sys.argv[1]
    index  = sys.argv[2]
    doc = sys.argv[3]
    if(filename.split(".")[-1] == "json"):
        a.insert_data_by_bulk(a.json_iterator(filename, index, doc))
    elif (filename.split(".")[-1] == "csv") and sys.argv[4]==1:
        a.insert_data_by_bulk(a.csv_iterator(filename,["CriterionID","Category"],index, doc))
    elif (filename.split(".")[-1] == "csv") and sys.argv[4]==2:
        a.insert_data_by_bulk(a.csv_iterator(filename,["username","password","role"],index, doc))
        