from typing import List, Optional


class QueryStatement:
    @staticmethod
    def getAll(size:int,sort:Optional[dict]=None):
        GET_ALL ={
            "size":size,
            "query": {
                "bool":{
                    "must":
                    [
                        {"match_all": {}}
                    ]
                }
            },
            "sort" : [
            ]            
        }
        if sort !=None:
            GET_ALL=GET_ALL["sort"].append(sort)
        return  GET_ALL

    @staticmethod

    def getAllByCondition(size:int,conditon:list,sort=None):
        GET_Condition ={
            "size":size,
            "query": {
                "bool":{
                    "must":
                    [
                        
                    ]
                }
            },
            "sort" : [
            ]            
        }
        GET_Condition["query"]["bool"]["must"]=GET_Condition["query"]["bool"]["must"]+conditon
        if sort !=None:
            GET_Condition["sort"]=GET_Condition["sort"]+sort
        return GET_Condition

    @staticmethod

    def getPageByCondition(size:int,startDoc:int,conditon:list,sort:Optional[dict]=None):
        GET_Condition ={
            "from":startDoc,
            "size":size,
            "query": {
                "bool":{
                    "must":
                    [
                    ]
                }
            },
            "sort" : [
            ]            
        }
        GET_Condition["query"]["bool"]["must"]=GET_Condition["query"]["bool"]["must"]+conditon
        if sort !=None:
            GET_Condition["sort"]=GET_Condition["sort"]+sort
        print(GET_Condition)
        return GET_Condition
    @staticmethod

    def getCount(conditon:list):
        GET_ALL ={
            "query": {
                "bool":{
                    "must":
                    [
                    ]
                }
            },          
        }
        GET_ALL["query"]["bool"]["must"]=GET_ALL["query"]["bool"]["must"]+conditon

        return  GET_ALL

