from typing import List, Optional
from settings import HOST
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from settings import *
class QueryBase:

    def __init__(self):
        self.host =HOST
        self.port = PORT
        if HASPASS:
            self.username = USER_NAME
            self.password = USER_PASS
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port ,'timeout':600}],http_auth=(self.username, self.password))
        else:
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port ,'timeout':600}])
    def reset(self):
        if HASPASS:
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port ,'timeout':600}],http_auth=(self.username, self.password))
        else:
            self.elasticsearch=Elasticsearch([{'host':self.host,'port':self.port ,'timeout':600}])
        
    def QuerySearch(self, QueryBody:str, index:str,filter_path:Optional[list]=None, doctype:Optional[str]=None):
        for i in range(2):
            try:
                data = self.elasticsearch.search(body = QueryBody,index = index,filter_path=filter_path)
                return data
            except:
                self.reset()
    def QueryCount(self, QueryBody:str, index:str):
        for i in range(2):
            try:
                data = self.elasticsearch.count(body = QueryBody,index = index)
                return data
            except:
                self.reset()
    def getinfo(self):
        print(self.elasticsearch.indices.get_alias("*"))