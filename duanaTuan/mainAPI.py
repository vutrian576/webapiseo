from typing import List, Optional

from pydantic.typing import resolve_annotations
from settings import *
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from settings import *
import os, uuid
import pandas as pd
import csv
import time
from QueryStatement import QueryStatement
from QueryBase import *
from ImportData import *
from API import *
from LoginController import *
import fastapi
from fastapi import Depends, FastAPI, HTTPException, status,File,Form,Request,Response
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.responses import FileResponse
import zipfile
import uvicorn
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
private = FastAPI()
public = FastAPI()

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
def get_current_user(token):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        role: str = payload.get("role")
        if username is None:
            raise credentials_exception
    except:
        raise credentials_exception
    print("bbbb")
    return username,role
@private.middleware("http")
async def add_process_time_header(request: Request, call_next):
    response = Response(status_code=401,content="Unauthorized")
    try:
        a=get_current_user(request.headers["access_token"])
    except:
        return response
    b = await call_next(request)
    return b


loginController = LoginController()
api = API()
class CategoryData(BaseModel):
    PageStart:str
    sizePage:str
    nameCategories:Optional[str]=None
class KeywordData(BaseModel):
    CategoriesID:str
    PageStart:str
    PageEnd:Optional[str]=None
    sizePage:str
    TextInKW:Optional[str]=None
    sortByKey:Optional[str]=None
    sortType:Optional[str]=None
    RangeField:Optional[str]=None
    RangeStart:Optional[str]=None
    RangeEnd:Optional[str]=None

#api
@public.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = loginController.authenticate_user(form_data.username, form_data.password)
    print(user)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = loginController.create_access_token(
        data={"sub": user["username"],"role":user["role"]}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}




@private.post("/categories")
async def getCategories(data:CategoryData):

    if data.nameCategories=="":
        a = api.getCategories(int(data.PageStart),int(data.sizePage),None,None)
    else:
        a = api.getCategories(int(data.PageStart),int(data.sizePage),None,data.nameCategories)

    if type(a) != bool :
        b = {"count":a[1]}
        a1=a[0].to_json(orient="records",force_ascii=False)
        b["data"] = a1

        return b
    else:
        raise HTTPException(status_code=400, detail="Not Found")

@private.post("/keyword")
async def getKeyword(data:KeywordData):
    if data.CategoriesID =="all":
        data.CategoriesID = "-1"
    sortByID = None

    if data.sortByKey!=None:
        sortByID = [{data.sortByKey:data.sortType}]
    if data.TextInKW =='':
        data.TextInKW =None
    a =  api.getKeyword(int(data.CategoriesID),int(data.PageStart),int(data.sizePage),None,data.TextInKW,sortByID,data.RangeField,data.RangeStart,data.RangeEnd)
    print(a)
    if type(a) != bool :
        b = {"count":a[1]}
        a1=a[0].to_json(orient="records",force_ascii=False)
        b["data"] = a1
        

        return b
    else:
        raise HTTPException(status_code=400, detail="Not Found")

@private.post("/getfilekeyword")
async def getFileKeyword(data:KeywordData):
    if (int(data.PageEnd) - int(data.PageStart) + 1)*int(data.sizePage)>50000:
        raise HTTPException(status_code=408, detail="over size")

    if data.CategoriesID =="all":
        data.CategoriesID = "-1"
    sortByID = None
    if data.sortByKey!=None:
        sortByID = [{data.sortByKey:data.sortType}]

    a =  api.getKeyword(int(data.CategoriesID),int(data.PageStart),int(data.sizePage),int(data.PageEnd),data.TextInKW,sortByID,data.RangeField,data.RangeStart,data.RangeEnd)
    print(a)
    if type(a) != bool :
        a=a[0].to_csv("dataqueryfile/XuatFile.csv",encoding="utf-8-sig")
        zipfile.ZipFile('dataqueryfile/XuatFile.zip',"w",zipfile.ZIP_DEFLATED).write("dataqueryfile/XuatFile.csv")

        return FileResponse("dataqueryfile/XuatFile.zip")
    else:
        raise HTTPException(status_code=400, detail="Not Found")
app.mount("/private", private)
app.mount("/public", public)
if __name__ == "__main__":


    uvicorn.run("mainAPI:app",host="0.0.0.0", port=8010,reload=True)