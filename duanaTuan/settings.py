from typing import Collection


HOST = "localhost"
PORT = 9200
HASPASS = True
USER_NAME = ""
USER_PASS = ""
COLLECTIONS = {
    "Accounts":"accounts",
    "Categories":"categories",
    "KeywordDetails":"keywords"
}
FieldSearch = {
    "Account":["hits.hits._source.username","hits.hits._source.password","hits.hits._source.role"],
    "Categories":["hits.hits._source.CriterionID","hits.hits._source.Category"],
    "KeywordDetails":["hits.hits._source.keyword","hits.hits._source.location","hits.hits._source.keyword_info.cpc","hits.hits._source.keyword_info.time_update",\
       "hits.hits._source.impressions_info.daily_impressions_avg" ,"hits.hits._source.impressions_info.daily_impressions_min",\
         "hits.hits._source.impressions_info.daily_impressions_max" ,"hits.hits._source.impressions_info.daily_clicks_avg", \
             "hits.hits._source.impressions_info.daily_clicks_min" ,"hits.hits._source.impressions_info.daily_clicks_max",\
                 "hits.hits._source.keyword_info.search_volume"
    ]
    }
#login
SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30