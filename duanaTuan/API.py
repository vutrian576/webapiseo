from typing import List, Optional

from fastapi.routing import APIRoute
from settings import HOST
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from settings import *
import os, uuid
import pandas as pd
import csv
import time
from QueryStatement import QueryStatement
from QueryBase import *
from ImportData import *

from pandas.io.json import json_normalize
class API:
    def __init__(self) -> None:
        self.QR = QueryBase()
        self.ID = ImportData()

    def getCategories(self ,pageStart , size,  pageEnd:Optional[int]=None, condition:Optional[str]=None, sort:Optional[int]=None):
        if condition == None:
            count =self.QR.QueryCount(QueryStatement.getCount([{"match_all":{}}]),COLLECTIONS["Categories"])["count"]
        else:
            count =self.QR.QueryCount(QueryStatement.getCount([{"match_phrase":{"Category":condition}}]),COLLECTIONS["Categories"])["count"]
        print(count)
        if count<(pageStart-1)*size and pageStart<1:
            return False
        if pageEnd!=None:
            if pageEnd<pageStart:
                return False
            else:
                startDoc = (pageStart-1)*size
                size = (pageEnd-pageStart+1)*size
        else:
            startDoc = (pageStart-1)*size
        if condition == None:
            QS = QueryStatement.getPageByCondition(size,startDoc,[{"match_all":{}}],sort)
        else:
            QS = QueryStatement.getPageByCondition(size,startDoc,[{"match_phrase":{"Category":condition}}],sort)
        ac=self.QR.QuerySearch(QS,index=COLLECTIONS["Categories"],filter_path=FieldSearch["Categories"])
        if len(ac)>0:
            dfItem = json_normalize(ac['hits']['hits'])
            index =  [i.split(".")[-1] for i in  dfItem.columns]
            dfItem.columns =index
            print(dfItem)
            return dfItem, count
        else:
            return False

    def getKeyword(self, condition_categories, pageStart , size,  pageEnd:Optional[int]=None, condition_keyword:Optional[str]=None, sort:Optional[list]=None,RangeField:Optional[str]=None,RangeStart:Optional[float]=None,RangeEnd:Optional[float]=None,):
        if condition_categories == -1:
            condition = []
        else:
            condition = [{"term":{"keyword_info.categories":condition_categories}}]
        if condition_keyword != None:
            condition = condition +[{"match_phrase":{"keyword":condition_keyword}}]
        if RangeField != None and RangeStart!=None and RangeEnd!=None:
            condition = condition +[{
                "range" : {
                RangeField : { "gte" : float(RangeStart), "lte" : float(RangeEnd) }
                }
                 }]
        if len(condition)==0:
            condition = [{"match_all":{}}]
        count =self.QR.QueryCount(QueryStatement.getCount(condition),COLLECTIONS["KeywordDetails"])["count"]
        if count<(pageStart-1)*size or pageStart<1:
            return False
        if pageEnd!=None:
            if pageEnd<pageStart:
                return False
            else:
                startDoc = (pageStart-1)*size
                size = (pageEnd-pageStart+1)*size
        else:
            startDoc = (pageStart-1)*size

        QS = QueryStatement.getPageByCondition(size,startDoc,condition,sort)
        ac=self.QR.QuerySearch(QS,index=COLLECTIONS["KeywordDetails"],filter_path=FieldSearch["KeywordDetails"],)

        if len(ac)>0:
            dfItem = json_normalize(ac['hits']['hits'])
            index =  [i.split(".")[-1] for i in  dfItem.columns]
            dfItem.columns =index
            return dfItem, count
        else:
            return False