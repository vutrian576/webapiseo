from datetime import datetime, time, timedelta
from typing import Optional
import fastapi
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from elasticsearch import Elasticsearch
from pydantic import BaseModel
from settings import *
from QueryStatement import QueryStatement
from QueryBase import *
# to get a string like this run:
# openssl rand -hex 32


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class User(BaseModel):
    username: str
    role: Optional[str] = None



class LoginController:
    def __init__(self) -> None:  
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self.oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

    def authenticate_user(self,username: str, password: str):
        c= QueryBase()
        h =QueryStatement.getAllByCondition(1,[{"term":{"username":username}},{"term":{"password":password}}])

        try:
            bc=c.QuerySearch(h,index=COLLECTIONS["Accounts"],filter_path=FieldSearch["Account"])
            bc= {
                "username": bc["hits"]["hits"][0]["_source"]["username"],
                "role":bc["hits"]["hits"][0]["_source"]["role"]
            }
            return bc
        except:
            return False

    def create_access_token(self, data: dict, expires_delta: Optional[timedelta] = None):
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=300)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
        return encoded_jwt


    def login_for_access_token(self,form_data: OAuth2PasswordRequestForm = Depends()):
        user = self.authenticate_user(form_data.username, form_data.password)
        if not user:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect username or password",
                headers={"WWW-Authenticate": "Bearer"},
            )
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = self.create_access_token(
            data={"sub": user.username,"role":user.role}, expires_delta=access_token_expires
        )
        return {"access_token": access_token, "token_type": "bearer"}